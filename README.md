# bugzilla2bitbucket

bugzilla2bitbucket is a Java application for transferring bugs from a Bugzilla server to an issue tracker in Bitbucket.
It is open source and free to use and modify, see [LICENSE.txt](LICENSE.txt).

## Limitations

* Does not transfer versions, milestones or bug history
* Importing issue watchers is not enabled because of [this Bitbucket limitation](https://bitbucket.org/site/master/issues/7417/issue-importer-does-not-transfer-watchers)
* Only tested with Bugzilla 4.4

## Instructions

### Set up environment

Download [bugzilla2bitbucket.jar](https://bitbucket.org/swedishnationaldataservice/bugzilla-to-bitbucket/downloads/bugzilla2bitbucket.jar) 
and verify that you can run it by invoking `java -jar bugzilla2bitbucket.jar`.

Make sure that XML RPC is enabled on the Bugzilla server.

### Export XML file from Bugzilla

Export the list of bugs that you want to import to a Bitbucket repo by using the XML
export capability of Bugzilla.

For this you can use `Advanced search` in the Bugzilla UI, and choose for example all bugs belonging to a
product.

In the search result, make sure that all results are displayed. If not, click `See all results for this query` on
the bottom of the page.

Once all bugs you want to import are present in the result page, click on the `XML` button on the bottom of the page
and save the resulting XML file as e.g. `bugs.xml`.

### Optional: Create components.txt

By default, any components encountered in the XML file will be recreated with the same name in Bitbucket.
To configure component names it is possible to map the Bugzilla component names to new component names in Bitbucket. To do this,
create an UTF-8 encoded text file named `components.txt` in the same directory as `bugzilla2bitbucket.jar`.
Each row in the file defines one mapping from a Bugzilla component to a Bitbucket component separated with `=`.
It is possible to remove a component by specifiying e.g. `Component3=`. In order to differentiate between components
with the same name in different products, it is possible to specify both the product and the component in the form
of `ProductA.ComponentA=ComponentB`. Product-specific mappings take precedence over mappings without a specified
product.

Any `=` or `.` in product or component names can be escaped with a backlash, e.g. `Product\=A\.B.ComponentA=ComponentC` means
that product `Product=A.B` and its component `ComponentA` will be mapped to `ComponentC`.

Example:

```
ProductA.ComponentA=ComponentB
ProductB.ComponentA=ComponentC
Component1=Component2
Component3=
```

### Optional: Create users.txt

It is recommended that you create a file containing mappings from Bugzilla usernames to Bitbucket usernames.
Any username that is not mapped to a Bitbucket user will be displayed as "Anonymous" in Bitbucket (it is
silently replaced with `null` by the importer).

To do this, create an UTF-8 encoded text file named `users.txt` in the same directory as `bugzilla2bitbucket.jar`.  
Each row in the file defines one mapping from a Bugzilla username to a Bitbucket username separated with `=`.

Example:

```
user1@bugzilla.com=bb_user1
user2@bugzilla.com=bb_user2
```

### Run bugzilla2bitbucket

Run `bugzilla2bitbucket` and supply username, password and URL to access the Bugzilla server (needed for downloading
attachments which are not included in the XML export):

`java -jar bugzilla2bitbucket.jar -blogin username -bpwd password -burl url-to-bugzilla bugs.xml`

This will create the file `bitbucket.zip` (use the `-o` parameter to specify another file name).

In order to generate new issue identifiers (from 1 upwards), use the `-ids` parameter. Default is to use
the identifiers from Bugzilla.

### Import into Bitbucket repo (**WARNING: deletes all existing issues, versions, milestones and issue tracker settings!**)

Go to `Settings->Issues->Import & export` in the Bitbucket repo, choose the `bitbucket.zip` file and press
`Start import`. Please note that this deletes all existing issues, versions, milestones and issue tracker settings!