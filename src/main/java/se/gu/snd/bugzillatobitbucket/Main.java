package se.gu.snd.bugzillatobitbucket;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.j2bugzilla.base.BugzillaException;
import com.j2bugzilla.base.ConnectionException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        Configuration configuration = new Configuration();
        JCommander commander = new JCommander(configuration);
        commander.setProgramName("bugzilla2bitbucket");

        try {
            commander.parse(args);
        } catch (ParameterException e) {
            commander.usage();
            return;
        }

        main.run(configuration);
    }

    public void run(Configuration configuration) {
        try {
            configuration.readComponentsFile();
            configuration.readUsersFile();

            System.out.println("Creating " + configuration.getOutFile() + "...");

            Converter.convert(configuration.getXmlFile(), configuration);
        } catch (BugzillaException | ConnectionException | IOException e) {
            System.err.println("Conversion failed with exception " + e);
            System.exit(1);
        }

        System.out.println("Finished.");
        System.exit(0);
    }

}
