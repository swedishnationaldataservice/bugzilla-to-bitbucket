package se.gu.snd.bugzillatobitbucket;

import java.util.Objects;

public class BugzillaComponent {

    private final String product;
    private final String component;

    public BugzillaComponent(String product, String component)
    {
        this.product = product;
        this.component = component;
    }
    
    public String getProduct() {
        return product;
    }

    public String getComponent() {
        return component;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null)
            return false;
        if (!(o instanceof BugzillaComponent))
            return false;
        
        BugzillaComponent c = (BugzillaComponent)o;
        
        return component.equalsIgnoreCase(c.getComponent()) && product.equalsIgnoreCase(getProduct());        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(product);
        hash = 23 * hash + Objects.hashCode(component);
        return hash;
    }
}
