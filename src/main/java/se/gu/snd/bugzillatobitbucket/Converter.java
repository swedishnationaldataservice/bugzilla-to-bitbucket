package se.gu.snd.bugzillatobitbucket;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j2bugzilla.base.BugFactory;
import com.j2bugzilla.base.BugzillaConnector;
import com.j2bugzilla.base.BugzillaException;
import com.j2bugzilla.base.ConnectionException;
import com.j2bugzilla.rpc.GetAttachments;
import com.j2bugzilla.rpc.LogIn;
import com.j2bugzilla.rpc.LogOut;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import se.gu.snd.bugzillatobitbucket.bitbucket.Attachment;
import se.gu.snd.bugzillatobitbucket.bitbucket.BitbucketIssues;
import se.gu.snd.bugzillatobitbucket.bitbucket.Comment;
import se.gu.snd.bugzillatobitbucket.bitbucket.Component;
import se.gu.snd.bugzillatobitbucket.bitbucket.Issue;
import se.gu.snd.bugzillatobitbucket.bitbucket.Meta;
import se.gu.snd.bugzillatobitbucket.bugzilla.Bug;
import se.gu.snd.bugzillatobitbucket.bugzilla.Bugzilla;
import se.gu.snd.bugzillatobitbucket.bugzilla.BugzillaReader;
import se.gu.snd.bugzillatobitbucket.bugzilla.LongDesc;

public class Converter {

    public static void convert(String bugzillaXmlFile, Configuration configuration) throws BugzillaException, ConnectionException, IOException {
        Bugzilla bugzilla = BugzillaReader.getFromFile(bugzillaXmlFile);
        BugzillaConnector connector = getBugzillaConnector(configuration);
        File outFile = new File(configuration.getOutFile());
        FileUtils.forceMkdirParent(outFile);

        BitbucketIssues result = new BitbucketIssues();
        result.setMeta(getMeta());
        Set<String> foundComponents = new HashSet<>();
        Map<String, Integer> idMap = buildIdMap(bugzilla.getBug(), configuration.getGenerateIds());

        try (ZipOutputStream out = new ZipOutputStream(
                new BufferedOutputStream(new FileOutputStream(outFile)))) {

            for (Bug bug : bugzilla.getBug()) {
                Issue issue = getIssue(bug, configuration, foundComponents, idMap);
                result.getIssues().add(issue);
                result.getComments().addAll(getIssueComments(bug, issue.getId(), configuration, idMap));
                result.getAttachments().addAll(
                    getIssueAttachments(connector, Integer.parseInt(bug.getBugId()),
                        issue.getId(), configuration, out));

                System.out.println("#" + issue.getId() + " " + issue.getTitle());
            }

            result.setComponents(getComponents(foundComponents));

            writeJson(result, out);

        } finally {
            closeBugzillaConnector(connector);
        }
    }

    private static Map<String, Integer> buildIdMap(List<Bug> bugs, boolean generate) {
        HashMap<String, Integer> result = new HashMap<>();

        for (int i = 0; i < bugs.size(); i++) {
            String id = bugs.get(i).getBugId();

            if (generate) {
                result.put(id, i + 1);
            } else {
                result.put(id, Integer.parseInt(id));
            }
        }

        return result;
    }

    private static Issue getIssue(Bug bug, Configuration conf, Set<String> components, Map<String, Integer> idMap) {
        Issue i = new Issue();

        i.setAssignee(conf.getBitbucketUser(bug.getAssignedTo().getvalue()));

        String component = conf.getBitbucketComponent(bug.getProduct(), bug.getComponent());
        if (component.isEmpty()) {
            i.setComponent(null);
        } else {
            i.setComponent(component);
            components.add(i.getComponent());
        }
        
        LongDesc firstComment = getFirstComment(bug);
        i.setContentUpdatedOn(toISO8601(firstComment.getBugWhen()));

        String reporterName = getBugzillaName(conf, bug.getReporter().getvalue(),
                bug.getReporter().getName(), i.getContentUpdatedOn());

        i.setContent(reporterName + toBitbucketMarkdown(firstComment.getThetext(), i.getContentUpdatedOn(), idMap));

        i.setCreatedOn(toISO8601(bug.getCreationTs()));
        i.setEditedOn(null);
        i.setId(idMap.get(bug.getBugId()));
        i.setKind(getIssueKind(bug));
        i.setMilestone(null); // could use bug.getTargetMilestone(), needs to be add in bitbucket milestones array
        i.setPriority(getIssuePriority(bug));
        i.setReporter(conf.getBitbucketUser(bug.getReporter().getvalue()));
        i.setStatus(getIssueStatus(bug));
        i.setTitle(abbreviateString(bug.getShortDesc(), 255));
        i.setUpdatedOn(toISO8601(bug.getDeltaTs()));
        i.setVersion(null); // could use bug.getVersion(), needs to be added in bitbucket versions array

        /*
        * Setting watchers does not actually do anything since it is disabled
        * by Bitbucket, see https://bitbucket.org/site/master/issues/7417/issue-importer-does-not-transfer-watchers.
         */
        i.setWatchers(getIssueWatchers(bug, conf));

        i.setVoters(null);

        return i;
    }

    private static LongDesc getFirstComment(Bug bug) {
        return bug.getLongDesc().stream().filter(x -> "0".equals(x.getCommentCount()))
                .findFirst().get();
    }

    private static String getIssueKind(Bug bug) {
        // Bugzilla values for severity:
        // blocker
        // critical
        // major
        // normal
        // minor
        // trivial       
        // enhancement

        switch (bug.getBugSeverity()) {
            case "enhancement":
                return "enhancement";
            default:
                return "bug";
        }

    }

    private static String getIssuePriority(Bug bug) {
        // Bugzilla values for severity:
        // blocker
        // critical
        // major
        // normal
        // minor
        // trivial
        // enhancement

        switch (bug.getBugSeverity()) {
            case "normal":
                return "minor"; // Or "major"? Or use bug.getPriority() somehow?
            case "enhancement":
                return "minor"; // Maybe use bug.getPriority()?
            default:
                return bug.getBugSeverity();
        }
    }

    private static String getIssueStatus(Bug bug) {
        // Bugzilla values for status:
        // UNCONFIRMED
        // NEW
        // CONFIRMED
        // ASSIGNED
        // REOPENED
        // RESOLVED
        // VERIFIED
        // CLOSED

        // Bugzilla values for resolution:
        // FIXED
        // INVALID
        // WONTFIX
        // DUPLICATE
        // WORKSFORME
        // MOVED
        switch (bug.getBugStatus()) {
            case "UNCONFIRMED":
                return "new";
            case "NEW":
                return "new";
            case "CONFIRMED":
                return "open";
            case "REOPENED":
                return "open";
            case "VERIFIED":
                return "open";
            case "CLOSED":
                return "resolved";
            case "ASSIGNED":
                return "open";
            case "RESOLVED":
                switch (bug.getResolution()) {
                    case "FIXED":
                        return "resolved";
                    case "INVALID":
                        return "invalid";
                    case "WONTFIX":
                        return "wontfix";
                    case "DUPLICATE":
                        return "duplicate";
                    case "WORKSFORME":
                        return "resolved"; // Or "wontfix", "invalid"?
                    case "MOVED":
                        return "resolved";
                }

            default:
                return "new";
        }
    }

    private static List<String> getIssueWatchers(Bug bug, Configuration conf) {
        if (bug.getCc().isEmpty()) {
            return null;
        }

        return bug.getCc().stream().map(x -> conf.getBitbucketUser(x.getvalue()))
                .collect(Collectors.toList());
    }

    private static List<Comment> getIssueComments(Bug bug, int issueId, Configuration conf, Map<String, Integer> idMap) {
        List<Comment> result = new ArrayList<>();
        int id = 1;

        for (LongDesc comment : bug.getLongDesc()) {
            if ("0".equals(comment.getCommentCount())) {
                continue;
            }

            Comment c = new Comment();
            c.setCreatedOn(toISO8601(comment.getBugWhen()));

            String commenter = getBugzillaName(conf, comment.getWho().getvalue(),
                    comment.getWho().getName(), c.getCreatedOn());

            c.setContent(commenter + toBitbucketMarkdown(comment.getThetext(), c.getCreatedOn(), idMap));

            c.setId(id++);
            c.setIssue(issueId);
            c.setUpdatedOn(null);
            c.setUser(conf.getBitbucketUser(comment.getWho().getvalue()));

            result.add(c);
        }

        return result;
    }

    private static List<Attachment> getIssueAttachments(BugzillaConnector connector,
            int bugzillaId, int issueId, Configuration conf, ZipOutputStream out) throws BugzillaException, IOException {
        List<Attachment> result = new ArrayList<>();

        for (com.j2bugzilla.base.Attachment attachment : getBugzillaAttachments(connector, bugzillaId)) {
            Attachment a = new Attachment();
            String path = "attachments/" + attachment.getAttachmentID() + "-" + attachment.getFileName();

            a.setFilename(attachment.getFileName());
            a.setIssue(issueId);
            a.setPath(path);
            a.setUser(conf.getBitbucketUser(attachment.getCreator()));

            result.add(a);

            out.putNextEntry(new ZipEntry(path));
            out.write(attachment.getRawData());
            out.flush();
            out.closeEntry();
        }

        return result;
    }

    private static Meta getMeta() {
        // Should this be configurable?

        Meta meta = new Meta();
        meta.setDefaultAssignee(null);
        meta.setDefaultComponent(null);
        meta.setDefaultKind("bug");
        meta.setDefaultMilestone(null);
        meta.setDefaultVersion(null);

        return meta;
    }

    private static List<Component> getComponents(Set<String> foundComponents) {
        return foundComponents.stream().map(c -> {
            Component r = new Component();
            r.setName(c);
            return r;
        }).collect(Collectors.toList());
    }

    private static BugzillaConnector getBugzillaConnector(Configuration conf) throws BugzillaException, ConnectionException {
        BugzillaConnector conn = new BugzillaConnector();
        conn.connectTo(conf.getBugzillaURL());

        LogIn login = new LogIn(conf.getBugzillaLogin(), conf.getBugzillaPassword());
        conn.executeMethod(login);

        conn.setToken(login.getToken());

        return conn;
    }

    private static void closeBugzillaConnector(BugzillaConnector connector) {
        try {
            connector.executeMethod(new LogOut());
        } catch (BugzillaException e) {
        }
    }

    private static List<com.j2bugzilla.base.Attachment> getBugzillaAttachments(BugzillaConnector connector, int bugzillaId) throws BugzillaException {
        BugFactory factory = new BugFactory();
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", bugzillaId);
        map.put("product", "x");
        map.put("component", "x");
        map.put("summary", "x");
        map.put("version", "x");
        com.j2bugzilla.base.Bug bug = factory.createBug(map);

        GetAttachments getAttachments = new GetAttachments(bug);
        connector.executeMethod(getAttachments);

        return getAttachments.getAttachments();
    }

    private static String toISO8601(String bugzillaDate) {
        // Bugzilla format 2012-10-11 14:02:56 +0200
        // ISO 8601 format 2012-10-11T14:02:56+02:00

        if (bugzillaDate == null || bugzillaDate.length() != 25) {
            return null;
        }

        StringBuilder result = new StringBuilder(bugzillaDate);
        result.setCharAt(10, 'T');
        result.setCharAt(19, bugzillaDate.charAt(20));
        result.setCharAt(20, bugzillaDate.charAt(21));
        result.setCharAt(21, bugzillaDate.charAt(22));
        result.setCharAt(22, ':');

        return result.toString();
    }

    private static String abbreviateString(String input, int maxLength) {
        if (input.length() <= maxLength) {
            return input;
        } else {
            return input.substring(0, maxLength - 2) + "..";
        }
    }

    private static String getBugzillaName(Configuration conf, String bugzillaUser,
            String bugzillaName, String updateDate) {

        if (!conf.hasMappingForUser(bugzillaUser)) {
            String name;
            if (bugzillaName == null || bugzillaName.trim().isEmpty()) {
                name = bugzillaUser;
            } else {
                name = bugzillaName.trim();
            }

            if (markdownIsUsable(updateDate)) {
                return "*" + name + ":*\n\n";
            } else {
                return name + ":\n\n";
            }
        }

        return "";
    }

    private static String toBitbucketMarkdown(String input, String updateDate, Map<String, Integer> idMap) {
        // Insert double spaces before single newlines
        String result = input.replaceAll("(?<!  )(?<!\\n)\\n(?!\\n)", "  \n");

        // Escape characters with special meaning in Markdown.
        if (markdownIsUsable(updateDate)) {
            result = result.replaceAll("([\\\\`\\*_\\{\\}\\[\\]\\(\\)#\\+\\-!\\>])", "\\\\$1");
        }

        // Convert "bug x"/"bugg x" to "issue #x"
        Pattern p = Pattern.compile("(?<![^\\s])(?i)(?:bug|bugg)(?-i)[ ]+(?<id>[0-9]+)");
        Matcher m = p.matcher(result);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            // Only convert if the issue number exists in this import
            if (idMap.containsKey(m.group("id"))) {
                m.appendReplacement(sb, "issue #" + idMap.get(m.group("id")));
            }
        }
        m.appendTail(sb);
        
        return sb.toString();
    }

    private static boolean markdownIsUsable(String updateDate) {
        // For some reason Markdown only works for issue content and comments that were
        // updated (or created if never updated) 2012-10-10 or later.
        // Probably due to some internal technical limitation at Bitbucket.
        LocalDate cutOffDate = LocalDate.of(2012, Month.OCTOBER, 9);

        return LocalDate.parse(updateDate.substring(0, 10)).isAfter(cutOffDate);
    }

    private static void writeJson(BitbucketIssues result, ZipOutputStream out) throws IOException {
        out.putNextEntry(new ZipEntry("db-1.0.json"));

        Gson gson = new GsonBuilder().serializeNulls().create();
        OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
        gson.toJson(result, writer);
        writer.flush();

        out.closeEntry();
    }
}
