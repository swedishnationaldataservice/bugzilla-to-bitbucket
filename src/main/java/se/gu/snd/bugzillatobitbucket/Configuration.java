package se.gu.snd.bugzillatobitbucket;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;

public class Configuration {

    private final Map<String, String> userMap = new HashMap<>();
    private final Map<BugzillaComponent, String> componentMap = new HashMap<>();
    
    @Parameter(names = { "--bugzillaLogin", "-blogin" }, description = "Username for Bugzilla server", required = true)
    private String bugzillaLogin;
    @Parameter(names = { "--bugzillaPwd", "-bpwd" }, description = "Password for Bugzilla server", required = true)
    private String bugzillaPassword;
    @Parameter(names = { "--bugzillaURL", "-burl" }, description = "Bugzilla server URL, e.g. https://example.com/bugzilla", required = true)
    private String bugzillaURL;
    @Parameter(names = { "--outFile", "-o" }, description = "The resulting zip file") 
    private String outFile = "bitbucket.zip";
    @Parameter(description = "bugs.xml", required = true)
    private List<String> xmlFile;
    @Parameter(names = "--usersFile", description = "File to read user mappings from (UTF-8 encoded)")
    private String usersFile = "users.txt";
    @Parameter(names = "--componentsFile", description = "File to read component mappings from (UTF-8 encoded)")
    private String componentsFile = "components.txt";
    @Parameter(names = { "--generateIdentifiers", "-ids" }, description = "Generate new issue identifiers (from 1 and upwards), default is keep bug identifiers")
    private boolean generateIds = false;
    
    public String getBitbucketUser(String bugzillaUser)
    {
        if (userMap.containsKey(bugzillaUser))
            return userMap.get(bugzillaUser);
        else
            return bugzillaUser;
    }
    
    public void addBitbucketUser(String bugzillaUser, String bitbucketUser)
    {
        userMap.put(bugzillaUser, bitbucketUser);
    }
    
    public boolean hasMappingForUser(String bugzillaUser) {
        return userMap.containsKey(bugzillaUser);
    }
    
    public String getBitbucketComponent(String bugzillaProduct, String bugzillaComponent)
    {
        BugzillaComponent c = new BugzillaComponent(bugzillaProduct, bugzillaComponent);
        BugzillaComponent c2 = new BugzillaComponent("", bugzillaComponent);
        
        if (componentMap.containsKey(c))
            return componentMap.get(c);
        else if (componentMap.containsKey(c2))
            return componentMap.get(c2);
        else
            return bugzillaComponent;
    }
    
    public void addBitbucketComponent(String bugzillaProduct, String bugzillaComponent, String bitbucketComponent)
    {
        componentMap.put(new BugzillaComponent(bugzillaProduct, bugzillaComponent), bitbucketComponent);
    }
    
    public void setBugzillaURL(String URL)
    {
        bugzillaURL = URL;
    }
    
    public String getBugzillaURL()
    {
        return bugzillaURL;
    }
    
    public void setBugzillaLogin(String login)
    {
        bugzillaLogin = login;
    }
    
    public String getBugzillaLogin()
    {
        return bugzillaLogin;
    }
    
    public void setBugzillaPassword(String password)
    {
        bugzillaPassword = password;
    }
    
    public String getBugzillaPassword()
    {
        return bugzillaPassword;
    }
    
    public void setOutFile(String fileName)
    {
        outFile = fileName;
    }
    
    public String getOutFile()
    {
        return outFile;
    }
    
    public String getXmlFile()
    {
        return xmlFile.get(0);
    }
    
    public boolean getGenerateIds()
    {
        return generateIds;
    }
    
    public void readUsersFile() throws IOException
    {
        File f = new File(usersFile);
        if (!f.exists())
        {
            System.out.println("INFO: users file not found");
            return;
        }
        
        List<String> lines = FileUtils.readLines(f, "UTF-8");
        for (String l : lines)
        {
            String[] values = l.split("=", 2);
            if (values.length != 2)
                throw new ParameterException("Can not parse users file");
            
            addBitbucketUser(values[0].trim(), values[1].trim());
        }      
    }
    
    public void readComponentsFile() throws IOException
    {
        File f = new File(componentsFile);
        if (!f.exists())
        {
            System.out.println("INFO: components file not found");
            return;
        }
        
        List<String> lines = FileUtils.readLines(f, "UTF-8");
        for (String l : lines)
        {
            String[] values = l.split("(?<!\\\\)=", 2);
            if (values.length != 2)
                throw new ParameterException("Can not parse components file");
            
            String[] productComponent = values[0].split("(?<!\\\\)\\.");
            String product;
            String component;
            
            if (productComponent.length == 2) {
                product = removeEscapes(productComponent[0].trim());
                component = removeEscapes(productComponent[1].trim());
            } else {
                product = "";
                component = removeEscapes(values[0].trim());
            }
            
            addBitbucketComponent(product, component, removeEscapes(values[1].trim()));
        }      
    }
    
    private static String removeEscapes(String input) {
        return input.replaceAll("\\\\=", "=").replaceAll("\\\\\\.", ".");
    }
}
