
package se.gu.snd.bugzillatobitbucket.bitbucket;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class BitbucketIssues {

    @SerializedName("issues")
    @Expose
    private List<Issue> issues = new ArrayList<Issue>();
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = new ArrayList<Comment>();
    @SerializedName("attachments")
    @Expose
    private List<Attachment> attachments = new ArrayList<Attachment>();
    @SerializedName("logs")
    @Expose
    private List<Log> logs = new ArrayList<Log>();
    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("components")
    @Expose
    private List<Component> components = new ArrayList<Component>();
    @SerializedName("milestones")
    @Expose
    private List<Milestone> milestones = new ArrayList<Milestone>();
    @SerializedName("versions")
    @Expose
    private List<Version> versions = new ArrayList<Version>();

    /**
     * 
     * @return
     *     The issues
     */
    public List<Issue> getIssues() {
        return issues;
    }

    /**
     * 
     * @param issues
     *     The issues
     */
    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    /**
     * 
     * @return
     *     The comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * 
     * @return
     *     The attachments
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * 
     * @param attachments
     *     The attachments
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * 
     * @return
     *     The logs
     */
    public List<Log> getLogs() {
        return logs;
    }

    /**
     * 
     * @param logs
     *     The logs
     */
    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    /**
     * 
     * @return
     *     The meta
     */
    public Meta getMeta() {
        return meta;
    }

    /**
     * 
     * @param meta
     *     The meta
     */
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    /**
     * 
     * @return
     *     The components
     */
    public List<Component> getComponents() {
        return components;
    }

    /**
     * 
     * @param components
     *     The components
     */
    public void setComponents(List<Component> components) {
        this.components = components;
    }

    /**
     * 
     * @return
     *     The milestones
     */
    public List<Milestone> getMilestones() {
        return milestones;
    }

    /**
     * 
     * @param milestones
     *     The milestones
     */
    public void setMilestones(List<Milestone> milestones) {
        this.milestones = milestones;
    }

    /**
     * 
     * @return
     *     The versions
     */
    public List<Version> getVersions() {
        return versions;
    }

    /**
     * 
     * @param versions
     *     The versions
     */
    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

}
