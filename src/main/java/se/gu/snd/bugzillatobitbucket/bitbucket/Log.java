
package se.gu.snd.bugzillatobitbucket.bitbucket;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Log {

    @SerializedName("changed_from")
    @Expose
    private String changedFrom;
    @SerializedName("changed_to")
    @Expose
    private String changedTo;
    @SerializedName("comment")
    @Expose
    private Integer comment;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("field")
    @Expose
    private String field;
    @SerializedName("issue")
    @Expose
    private Integer issue;
    @SerializedName("user")
    @Expose
    private String user;

    /**
     * 
     * @return
     *     The changedFrom
     */
    public String getChangedFrom() {
        return changedFrom;
    }

    /**
     * 
     * @param changedFrom
     *     The changed_from
     */
    public void setChangedFrom(String changedFrom) {
        this.changedFrom = changedFrom;
    }

    /**
     * 
     * @return
     *     The changedTo
     */
    public String getChangedTo() {
        return changedTo;
    }

    /**
     * 
     * @param changedTo
     *     The changed_to
     */
    public void setChangedTo(String changedTo) {
        this.changedTo = changedTo;
    }

    /**
     * 
     * @return
     *     The comment
     */
    public Integer getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    public void setComment(Integer comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * 
     * @param createdOn
     *     The created_on
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * 
     * @return
     *     The field
     */
    public String getField() {
        return field;
    }

    /**
     * 
     * @param field
     *     The field
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * 
     * @return
     *     The issue
     */
    public Integer getIssue() {
        return issue;
    }

    /**
     * 
     * @param issue
     *     The issue
     */
    public void setIssue(Integer issue) {
        this.issue = issue;
    }

    /**
     * 
     * @return
     *     The user
     */
    public String getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(String user) {
        this.user = user;
    }

}
