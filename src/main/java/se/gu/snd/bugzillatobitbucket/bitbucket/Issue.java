
package se.gu.snd.bugzillatobitbucket.bitbucket;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Issue {

    @SerializedName("assignee")
    @Expose
    private String assignee;
    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("content_updated_on")
    @Expose
    private String contentUpdatedOn;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("edited_on")
    @Expose
    private String editedOn;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("milestone")
    @Expose
    private String milestone;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("reporter")
    @Expose
    private String reporter;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("watchers")
    @Expose
    private List<String> watchers = new ArrayList<String>();
    @SerializedName("voters")
    @Expose
    private List<String> voters = new ArrayList<String>();

    /**
     * 
     * @return
     *     The assignee
     */
    public String getAssignee() {
        return assignee;
    }

    /**
     * 
     * @param assignee
     *     The assignee
     */
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    /**
     * 
     * @return
     *     The component
     */
    public String getComponent() {
        return component;
    }

    /**
     * 
     * @param component
     *     The component
     */
    public void setComponent(String component) {
        this.component = component;
    }

    /**
     * 
     * @return
     *     The content
     */
    public String getContent() {
        return content;
    }

    /**
     * 
     * @param content
     *     The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 
     * @return
     *     The contentUpdatedOn
     */
    public String getContentUpdatedOn() {
        return contentUpdatedOn;
    }

    /**
     * 
     * @param contentUpdatedOn
     *     The content_updated_on
     */
    public void setContentUpdatedOn(String contentUpdatedOn) {
        this.contentUpdatedOn = contentUpdatedOn;
    }

    /**
     * 
     * @return
     *     The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * 
     * @param createdOn
     *     The created_on
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * 
     * @return
     *     The editedOn
     */
    public String getEditedOn() {
        return editedOn;
    }

    /**
     * 
     * @param editedOn
     *     The edited_on
     */
    public void setEditedOn(String editedOn) {
        this.editedOn = editedOn;
    }

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The kind
     */
    public String getKind() {
        return kind;
    }

    /**
     * 
     * @param kind
     *     The kind
     */
    public void setKind(String kind) {
        this.kind = kind;
    }

    /**
     * 
     * @return
     *     The milestone
     */
    public String getMilestone() {
        return milestone;
    }

    /**
     * 
     * @param milestone
     *     The milestone
     */
    public void setMilestone(String milestone) {
        this.milestone = milestone;
    }

    /**
     * 
     * @return
     *     The priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * 
     * @param priority
     *     The priority
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * 
     * @return
     *     The reporter
     */
    public String getReporter() {
        return reporter;
    }

    /**
     * 
     * @param reporter
     *     The reporter
     */
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The updatedOn
     */
    public String getUpdatedOn() {
        return updatedOn;
    }

    /**
     * 
     * @param updatedOn
     *     The updated_on
     */
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * 
     * @return
     *     The version
     */
    public String getVersion() {
        return version;
    }

    /**
     * 
     * @param version
     *     The version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 
     * @return
     *     The watchers
     */
    public List<String> getWatchers() {
        return watchers;
    }

    /**
     * 
     * @param watchers
     *     The watchers
     */
    public void setWatchers(List<String> watchers) {
        this.watchers = watchers;
    }

    /**
     * 
     * @return
     *     The voters
     */
    public List<String> getVoters() {
        return voters;
    }

    /**
     * 
     * @param voters
     *     The voters
     */
    public void setVoters(List<String> voters) {
        this.voters = voters;
    }

}
