
package se.gu.snd.bugzillatobitbucket.bitbucket;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Meta {

    @SerializedName("default_assignee")
    @Expose
    private String defaultAssignee;
    @SerializedName("default_component")
    @Expose
    private String defaultComponent;
    @SerializedName("default_kind")
    @Expose
    private String defaultKind;
    @SerializedName("default_milestone")
    @Expose
    private String defaultMilestone;
    @SerializedName("default_version")
    @Expose
    private String defaultVersion;

    /**
     * 
     * @return
     *     The defaultAssignee
     */
    public String getDefaultAssignee() {
        return defaultAssignee;
    }

    /**
     * 
     * @param defaultAssignee
     *     The default_assignee
     */
    public void setDefaultAssignee(String defaultAssignee) {
        this.defaultAssignee = defaultAssignee;
    }

    /**
     * 
     * @return
     *     The defaultComponent
     */
    public String getDefaultComponent() {
        return defaultComponent;
    }

    /**
     * 
     * @param defaultComponent
     *     The default_component
     */
    public void setDefaultComponent(String defaultComponent) {
        this.defaultComponent = defaultComponent;
    }

    /**
     * 
     * @return
     *     The defaultKind
     */
    public String getDefaultKind() {
        return defaultKind;
    }

    /**
     * 
     * @param defaultKind
     *     The default_kind
     */
    public void setDefaultKind(String defaultKind) {
        this.defaultKind = defaultKind;
    }

    /**
     * 
     * @return
     *     The defaultMilestone
     */
    public String getDefaultMilestone() {
        return defaultMilestone;
    }

    /**
     * 
     * @param defaultMilestone
     *     The default_milestone
     */
    public void setDefaultMilestone(String defaultMilestone) {
        this.defaultMilestone = defaultMilestone;
    }

    /**
     * 
     * @return
     *     The defaultVersion
     */
    public String getDefaultVersion() {
        return defaultVersion;
    }

    /**
     * 
     * @param defaultVersion
     *     The default_version
     */
    public void setDefaultVersion(String defaultVersion) {
        this.defaultVersion = defaultVersion;
    }

}
