package se.gu.snd.bugzillatobitbucket.bugzilla;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class BugzillaReader {

    public static Bugzilla getFromFile(String xmlFile) throws IOException {
        return getFromStream(new BufferedInputStream(new FileInputStream(xmlFile)));
    }

    public static Bugzilla getFromStream(InputStream xml) throws IOException {
        try {
            JAXBContext jc = JAXBContext.newInstance(Bugzilla.class);

            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            spf.setFeature("http://xml.org/sax/features/validation", false);

            XMLReader xmlReader = spf.newSAXParser().getXMLReader();
            InputSource inputSource = new InputSource(xml);
            SAXSource source = new SAXSource(xmlReader, inputSource);

            Unmarshaller jaxbUnmarshaller = jc.createUnmarshaller();
            jaxbUnmarshaller.setSchema(null);

            Bugzilla result = (Bugzilla) jaxbUnmarshaller.unmarshal(source);
            // Sort in ascending bug id order
            result.getBug().sort((x, y) -> Integer.compare(Integer.parseInt(x.getBugId()), Integer.parseInt(y.getBugId())));
        
            return result;

        } catch (JAXBException | ParserConfigurationException | SAXException e) {
            throw new IOException();
        }
    }
}
