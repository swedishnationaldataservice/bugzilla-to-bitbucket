//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.09.08 at 01:22:26 PM CEST 
//


package se.gu.snd.bugzillatobitbucket.bugzilla;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the se.gu.snd.bugzillatobitbucket.bugzilla package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: se.gu.snd.bugzillatobitbucket.bugzilla
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Keywords }
     * 
     */
    public Keywords createKeywords() {
        return new Keywords();
    }

    /**
     * Create an instance of {@link Data }
     * 
     */
    public Data createData() {
        return new Data();
    }

    /**
     * Create an instance of {@link SeeAlso }
     * 
     */
    public SeeAlso createSeeAlso() {
        return new SeeAlso();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link Urlbase }
     * 
     */
    public Urlbase createUrlbase() {
        return new Urlbase();
    }

    /**
     * Create an instance of {@link Attacher }
     * 
     */
    public Attacher createAttacher() {
        return new Attacher();
    }

    /**
     * Create an instance of {@link CommentSortOrder }
     * 
     */
    public CommentSortOrder createCommentSortOrder() {
        return new CommentSortOrder();
    }

    /**
     * Create an instance of {@link Who }
     * 
     */
    public Who createWho() {
        return new Who();
    }

    /**
     * Create an instance of {@link Flag }
     * 
     */
    public Flag createFlag() {
        return new Flag();
    }

    /**
     * Create an instance of {@link Dependson }
     * 
     */
    public Dependson createDependson() {
        return new Dependson();
    }

    /**
     * Create an instance of {@link Bugzilla }
     * 
     */
    public Bugzilla createBugzilla() {
        return new Bugzilla();
    }

    /**
     * Create an instance of {@link Bug }
     * 
     */
    public Bug createBug() {
        return new Bug();
    }

    /**
     * Create an instance of {@link Exporter }
     * 
     */
    public Exporter createExporter() {
        return new Exporter();
    }

    /**
     * Create an instance of {@link Blocked }
     * 
     */
    public Blocked createBlocked() {
        return new Blocked();
    }

    /**
     * Create an instance of {@link Attachment }
     * 
     */
    public Attachment createAttachment() {
        return new Attachment();
    }

    /**
     * Create an instance of {@link AssignedTo }
     * 
     */
    public AssignedTo createAssignedTo() {
        return new AssignedTo();
    }

    /**
     * Create an instance of {@link Cc }
     * 
     */
    public Cc createCc() {
        return new Cc();
    }

    /**
     * Create an instance of {@link Reporter }
     * 
     */
    public Reporter createReporter() {
        return new Reporter();
    }

    /**
     * Create an instance of {@link QaContact }
     * 
     */
    public QaContact createQaContact() {
        return new QaContact();
    }

    /**
     * Create an instance of {@link LongDesc }
     * 
     */
    public LongDesc createLongDesc() {
        return new LongDesc();
    }

}
